import { Component, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  @ViewChild('filesInput') filesInput: ElementRef;
  files = [];
  ext = ['jpg', 'png', 'gif', 'bmp', 'jpeg', 'pdf', 'tiff', 'doc', 'docx'];
  onOpenFilesInput() {
    this.filesInput.nativeElement.click();
  }

  onFilesChange(file: FileList) {
    this.files = this.files.concat(file);
  }

  onFilesChangeInput(event) {

    let files = event.srcElement.files;
    let valid_files: Array<File> = [];
    let invalid_files: Array<File> = [];
    if (files.length > 0) {
      for (let i = 0; i < files.length; i++) {
        let ext = files[i].name.split('.')[files[i].name.split('.').length - 1];
        if (this.ext.lastIndexOf(ext) !== -1) {
          valid_files.push(files[i]);
        }
        else {
          invalid_files.push(files[i]);
        }
      }
    }

    if (invalid_files.length > 0) {
      let str = 'Следующие файлы являются невалидными: ';
      for (let i = 0; i < invalid_files.length; i++) {
        str = str + invalid_files[i].name;
        if (i != invalid_files.length - 1)
          str = str + ', '
        else
          str = str + '.'
      }
      alert(str);
    }

    for (let i = 0; i < valid_files.length; i++) {
      this.files.push(valid_files[i]);
    }
  }


  del(index) {
    console.log('123' + index);
    this.files.splice(index, 1);
  }
}
